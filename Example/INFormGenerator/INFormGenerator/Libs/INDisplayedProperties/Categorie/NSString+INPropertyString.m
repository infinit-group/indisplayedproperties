//
// Created by Nils Rohwedder on 17.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//
NSString *const IN_PROPERTY_STRING_SEPARATOR = @":";

#import "NSString+INPropertyString.h"
#import "INDisplayedPropertiesModel.h"


@implementation NSString (INPropertyString)


+ (NSString *)propertyStringWithClass:(Class <INDisplayedPropertiesModel>)class property:(NSString *)property {
    if (![NSString identifierForClass:class]) {
        return property;
    } else {
        return [NSString stringWithFormat:@"%@%@%@", [NSString identifierForClass:class], IN_PROPERTY_STRING_SEPARATOR, property];
    }
}

+ (NSString *)propertyString:(NSString *)property, ... {
    va_list args;
    va_start(args, property);
    NSMutableArray *properties = [@[] mutableCopy];

    for (NSString *arg = property; arg != nil; arg = va_arg(args, NSString*)) {
        [properties addObject: arg];
    }
    va_end(args);

    return [properties componentsJoinedByString:IN_PROPERTY_STRING_SEPARATOR];
}

+ (BOOL)propertyStringWithClassBelongsToClass:(Class)class property:(NSString *)propertyWithClass {
    NSArray *components = [propertyWithClass componentsSeparatedByString:IN_PROPERTY_STRING_SEPARATOR];
    if ([components count] <= 1) {
        return NO;
    }
    if ([[components objectAtIndex:0] isEqualToString:[NSString identifierForClass:class]]) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSString *)identifierForClass:(Class <INDisplayedPropertiesModel>)class {
    Class identifierClass = class;
    if (![identifierClass respondsToSelector:@selector(identifier)]) {
        return nil;
    } else {
        return [identifierClass performSelector:@selector(identifier)];
    }
}

+ (id)valueForPropertyString:(id <INDisplayedPropertiesModel>)propertyObject property:(NSString *)property {
    NSArray *propertyComponents = [property componentsSeparatedByString:IN_PROPERTY_STRING_SEPARATOR];
    if ([propertyComponents count] <= 1) {
        return nil;
    }
    if ([propertyComponents count] > 2) {
        NSString *propertyStringToObject = [propertyComponents objectAtIndex:1];
        id <INDisplayedPropertiesModel> extractedPropertyObject = [(NSObject *) propertyObject valueForKey:propertyStringToObject];
        NSMutableArray *newPropertyComponents = [@[] mutableCopy];
        for (NSString *propertyName in propertyComponents) {
            if (![propertyName isEqualToString:propertyStringToObject]) {
                [newPropertyComponents addObject:propertyName];
            }
        }
        return [NSString valueForPropertyString:extractedPropertyObject property:[newPropertyComponents componentsJoinedByString:IN_PROPERTY_STRING_SEPARATOR]];
    } else {
        return [(NSObject *) propertyObject valueForKey:[propertyComponents objectAtIndex:[propertyComponents count] - 1]];
    }
}

+ (void)updatePropertyForString:(id <INDisplayedPropertiesModel>)propertyObject property:(NSString *)property value:(id)value {
    NSArray *propertyComponents = [property componentsSeparatedByString:IN_PROPERTY_STRING_SEPARATOR];
    if ([propertyComponents count] <= 1) {
        return;
    }
    if ([propertyComponents count] > 2) {
        NSString *propertyStringToObject = [propertyComponents objectAtIndex:1];
        id <INDisplayedPropertiesModel> extractedPropertyObject = [(NSObject *) propertyObject valueForKey:propertyStringToObject];
        NSMutableArray *newPropertyComponents = [@[] mutableCopy];
        for (NSString *propertyName in propertyComponents) {
            if (![propertyName isEqualToString:propertyStringToObject]) {
                [newPropertyComponents addObject:propertyName];
            }
        }
        [NSString updatePropertyForString:extractedPropertyObject property:[newPropertyComponents componentsJoinedByString:IN_PROPERTY_STRING_SEPARATOR] value:value];
    } else {
        [(NSObject *) propertyObject setValue:value forKey:[propertyComponents objectAtIndex:[propertyComponents count] - 1]];
    }
}

@end