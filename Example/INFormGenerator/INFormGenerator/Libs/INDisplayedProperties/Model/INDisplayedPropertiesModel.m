//
// Created by Nils Rohwedder on 13.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import "INDisplayedPropertiesDelegate.h"
#import "INDisplayedPropertiesModel.h"
#import "NSString+INPropertyString.h"

@implementation INDisplayedPropertiesModel {

}

+ (NSString *)identifier {
    return NSStringFromClass([self class]);
}

+ (NSArray *)displayedProperties:(NSString *)property {
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

//get value of property
+ (id)valueForDisplayedProperty:(id <INDisplayedPropertiesModel>)propertyModel property:(NSString *)property {
    return [NSString valueForPropertyString:propertyModel property:property];
}

//set new value of property
+ (void)updateDisplayedProperty:(id <INDisplayedPropertiesModel>)propertyModel property:(NSString *)property value:(id)value {
    [NSString updatePropertyForString:propertyModel property:property value:value];
}

+ (NSString *)displayProperty:(NSString *)property {
    return [NSString propertyStringWithClass:[self class] property:property];
}
@end