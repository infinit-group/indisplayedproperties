//
// Created by Nils Rohwedder on 13.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INDisplayedPropertiesDelegate.h"
#import "INDisplayedPropertiesModel.h"

@protocol INDisplayedPropertiesModel <INDisplayedPropertiesDelegate>

+ (NSString *)identifier;
+ (id)valueForDisplayedProperty:(id <INDisplayedPropertiesModel>)propertyModel property:(NSString *)property;
+ (void)updateDisplayedProperty:(id <INDisplayedPropertiesModel>)propertyModel property:(NSString *)property value:(id)value;

@end

@interface INDisplayedPropertiesModel : NSObject<INDisplayedPropertiesModel>

+ (NSString *)displayProperty:(NSString *)property;
@end