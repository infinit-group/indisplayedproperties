//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class INFormFormatter;
@class INLabel;
@protocol INDisplayedPropertiesModel;

@interface INFormGeneratorHelper : NSObject

+ (UITextField *)propertyUITextFieldForKey:(id <INDisplayedPropertiesModel>)displayPropertiesObject key:(NSString *)key frame:(CGRect)frame;

+ (UILabel *)propertyUILabelForKey:(id <INDisplayedPropertiesModel>)displayPropertiesObject key:(NSString *)property frame:(CGRect)frame;

+ (INLabel *)createTitleLabel:(CGRect)frame formFormatter:(INFormFormatter *)formFormatter;

@end