//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <objc/runtime.h>
#import "INFormGeneratorHelper.h"
#import "INFormGeneratorViewTwoColumns.h"
#import "INLabel.h"
#import "INDisplayedPropertiesModel.h"
#import "INFormGeneratorViewFormatter.h"
#import "INFormFormatter.h"

@implementation INFormGeneratorHelper {

}

+ (UITextField *)propertyUITextFieldForKey:(id <INDisplayedPropertiesModel>)displayPropertiesObject key:(NSString *)property frame:(CGRect)frame{
    Class<INDisplayedPropertiesModel> class = [displayPropertiesObject class];
    id value = [class valueForDisplayedProperty:displayPropertiesObject property:property];
    NSString *text = [INFormGeneratorHelper stringValue:value];

    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:12];
    textField.text = text;
    if ([value isKindOfClass:[NSNumber class]]){
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeAlphabet;
    }

    return textField;
}

+ (UILabel *)propertyUILabelForKey:(id <INDisplayedPropertiesModel>)displayPropertiesObject key:(NSString *)property frame:(CGRect)frame{
    Class<INDisplayedPropertiesModel> class = [displayPropertiesObject class];
    NSString *text = [INFormGeneratorHelper stringValue:[class valueForDisplayedProperty:displayPropertiesObject property:property]];

    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.font = [UIFont systemFontOfSize:12];
    label.text = text;
    label.layer.borderWidth = 1.0f;
    label.layer.cornerRadius = 5.0f;
    label.layer.borderColor = [UIColor blackColor].CGColor;

    return label;
}

+ (NSString *)stringValue:(id)value {
    if ([value isKindOfClass:[NSNumber class]]) {
        NSNumber *numberValue = value;
        return [NSString stringWithFormat:@"%@", [numberValue stringValue]];
    } else {
        return [NSString stringWithFormat:@"%@", value];
    }
}

+ (INLabel *)createTitleLabel:(CGRect)frame formFormatter:(INFormFormatter *)formFormatter {
    return [INFormGeneratorHelper createLabel:frame formFormatter:formFormatter];
}

+ (INLabel *)createLabel:(CGRect)frame formFormatter:(INFormFormatter *)formFormatter{
    INLabel *label = [[INLabel alloc] initWithFrame:frame];
    label.font = formFormatter.font;
    label.backgroundColor = [UIColor clearColor];
    if (formFormatter && formFormatter.layer) {
        label.layer.borderColor = formFormatter.layer.borderColor;
        label.layer.borderWidth = formFormatter.layer.borderWidth;
        label.layer.cornerRadius = formFormatter.layer.cornerRadius;
    }

    return label;
}

@end