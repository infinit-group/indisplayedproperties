//
// Created by Nils Rohwedder on 12.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <Foundation/Foundation.h>

@protocol INDisplayedPropertiesModel;

typedef enum {
    show,
    edit,
    update
} INFormMode;

@interface INFormGeneratorView : UIScrollView {

}

@property (nonatomic, strong) NSMutableDictionary *propertyTextFields;
@property INFormMode currentMode;

- (void)addKeyboardObserver;

- (NSMutableArray *)detailsKeysFromDisplayedObject:(id <INDisplayedPropertiesModel>)displayedObject;
- (void)updateProperties:(id <INDisplayedPropertiesModel>)displayedObject;

- (void)setActiveTextElement:(UIView *)inActiveTextArea;

- (IBAction)interactionEdit:(id)sender;
- (IBAction)interactionUpdate:(id)sender;
- (IBAction)interactionCancel:(id)sender;

@end