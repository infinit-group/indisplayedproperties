//
// Created by Nils Rohwedder on 12.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <CoreGraphics/CoreGraphics.h>
#import "INDisplayedPropertiesModel.h"
#import "INFormGeneratorView.h"

@interface INFormGeneratorView () {
}

@property CGSize kbSize;
@property (nonatomic, strong) UIView *activeTextElement;

@end

@implementation INFormGeneratorView {

}

@synthesize propertyTextFields, currentMode, kbSize, activeTextElement;

- (void) addKeyboardObserver{
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (NSMutableArray *)detailsKeysFromDisplayedObject:(id<INDisplayedPropertiesModel>)displayedObject {
    Class class = [displayedObject class];
    if ([class respondsToSelector:@selector(displayedProperties:)]) {
        return [class performSelector:@selector(displayedProperties:) withObject:nil];
    }
    return [@[] mutableCopy];
}

- (void)updateProperties:(id<INDisplayedPropertiesModel>)displayedObject {
    NSArray *labelKeys = [self detailsKeysFromDisplayedObject:displayedObject];
    for (NSString *key in labelKeys) {
        Class <INDisplayedPropertiesModel> class = [displayedObject class];
        NSObject *propertyTextElement = [self.propertyTextFields objectForKey:key];
        if ([propertyTextElement respondsToSelector:@selector(text)]){
            NSString *text = [propertyTextElement performSelector:@selector(text)];
            [class updateDisplayedProperty:displayedObject property:key value:text];
        }
    }
}

- (void)setActiveTextElement:(UIView *)inActiveTextElement {
    activeTextElement = inActiveTextElement;
    [self scrollToTextView:self.activeTextElement];
}


- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    self.kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    if (activeTextElement) {
        [self scrollToTextView:activeTextElement];
    }
}

- (void)scrollToTextView:(UIView *)inActiveTextElement {
    CGPoint textFieldFrameAccordingToCurrentPosition = [inActiveTextElement.superview convertRect:inActiveTextElement.frame toView:self].origin;
    CGRect frameWithoutKeyboardFrame = CGRectMake(0, 0, self.frame.size.width, ((self.frame.size.height - self.kbSize.height) / 2));

    float dif = 0.0f;
    if (!CGRectContainsPoint(frameWithoutKeyboardFrame, textFieldFrameAccordingToCurrentPosition)) {
        dif = textFieldFrameAccordingToCurrentPosition.y - frameWithoutKeyboardFrame.size.height;
    }
    CGPoint scrollPoint = CGPointMake(0.0, dif);
    [self setContentOffset:scrollPoint animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self setContentOffset:scrollPoint animated:YES];
}

#pragma mark interaction methods
- (IBAction)interactionEdit:(id)sender {
    self.currentMode = edit;
}

- (IBAction)interactionUpdate:(id)sender {
    self.currentMode = update;
}

- (IBAction)interactionCancel:(id)sender {
    self.currentMode = show;
}

@end