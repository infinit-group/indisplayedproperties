//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INFormGeneratorViewFormatter.h"
#import "INFormGeneratorView.h"

@protocol INDisplayedPropertiesModel;

@interface INFormGeneratorViewTwoColumns : INFormGeneratorView<UIScrollViewDelegate>

@property id<INDisplayedPropertiesModel> displayedObject;

- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject;
- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter;

- (void)viewWillAppear;
@end