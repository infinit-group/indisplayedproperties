//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
#import "INFormGeneratorViewTwoColumns.h"
#import "INDisplayedPropertiesModel.h"
#import "INFormGeneratorViewFormatter.h"
#import "INFormGeneratorHelper.h"
#import "INLabel.h"
#import "INFormGeneratorViewOption.h"
#import "INFormFormatter.h"

@interface INFormGeneratorViewTwoColumns () {
}

@property INFormGeneratorViewFormatter *viewFormatter;
@property INFormGeneratorViewOption *viewOption;

@end

@implementation INFormGeneratorViewTwoColumns {

}

@synthesize displayedObject;

- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject {
    return [self initWithFrame:frame object:inDisplayedObject formatter:[INFormGeneratorViewFormatter defaultTwoColumnFormGeneratorViewFormatterWithFrame:frame]];
}

- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter{
    return [self initWithFrame:frame object:inDisplayedObject formatter:formatter option:[INFormGeneratorViewOption defaultFormGeneratorViewOption]];
}

- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter option:(INFormGeneratorViewOption *)option{
    self = [super initWithFrame:frame];
    if (self) {
        self.displayedObject = inDisplayedObject;
        self.viewFormatter = formatter;
        self.viewOption = option;
        self.currentMode = self.viewOption.initialFormMode;
        [self setUpView];
    }

    return self;
}

- (void)setUpView {
    self.delegate = self;
    [self setScrollEnabled:YES];

    if (!self.viewFormatter) {
        self.viewFormatter = [INFormGeneratorViewFormatter defaultTwoColumnFormGeneratorViewFormatterWithFrame:self.frame];
    }

    [self setBackgroundColor: self.viewFormatter.backgroundColor];

    [self setUpPropertiesSubviews];
    CGRect separationFrame = CGRectMake(self.viewFormatter.xBreak, 0, 1, self.viewFormatter.frame.size.height);
    [self drawSeparationLine:separationFrame];
}

- (void)setUpPropertiesSubviews {
    self.propertyTextFields = [@{} mutableCopy];

    CGSize labelSize = CGSizeMake(self.viewFormatter.titleFieldWidth, self.viewFormatter.textElementHeight);
    CGSize textFieldSize = CGSizeMake(self.viewFormatter.valueFieldWidth, self.viewFormatter.textElementHeight);
    float maxHeight = (labelSize.height > textFieldSize.height ? labelSize.height : textFieldSize.height);
    [self setUpView:[self detailsKeysFromDisplayedObject:self.displayedObject] labelSize:labelSize textFieldSize:textFieldSize maxHeight:maxHeight];
}

- (void)changePropertiesSubviews {
    for (UIView *view in self.subviews){
        [view removeFromSuperview];
    }

    if (self.currentMode == update){
        [self updateProperties:self.displayedObject];
        self.currentMode = show;
    }
    //finally renew view
    [self setUpPropertiesSubviews];
}

- (void)setUpView:(NSArray *)detailsKeys labelSize:(CGSize)labelSize textFieldSize:(CGSize)textFieldSize maxHeight:(float) maxHeight{
    float calculationY = self.viewFormatter.y;
    float calculationX = self.viewFormatter.x;
    for (NSString *key in detailsKeys) {
        INLabel *label = [INFormGeneratorHelper createTitleLabel:CGRectMake(calculationX, calculationY, labelSize.width, labelSize.height) formFormatter:[INFormFormatter defaultNoBorderFormFormatter]];
        label.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(key, nil)];

        UIView *valueField;
        CGRect frame = CGRectMake(calculationX + labelSize.width + self.viewFormatter.xSpace, calculationY, textFieldSize.width, textFieldSize.height);

        if (self.currentMode != show) {
            valueField = [INFormGeneratorHelper propertyUITextFieldForKey:self.displayedObject key:key frame:frame];
        } else {
            valueField = [INFormGeneratorHelper propertyUILabelForKey:self.displayedObject key:key frame:frame];
        }

        [self.propertyTextFields setObject:valueField forKey:key];

        [self addSubview:label];
        [self addSubview:valueField];

        calculationY += maxHeight + self.viewFormatter.ySpace;
        if (calculationY > self.viewFormatter.yBreak) {
            calculationY = self.viewFormatter.y;
            calculationX = self.viewFormatter.xBreak + self.viewFormatter.x;
        }
    }
}

- (void)drawSeparationLine:(CGRect)frame {
    UIView *lineView = [[UIView alloc] initWithFrame:frame];
    lineView.backgroundColor = [UIColor blackColor];
    [self addSubview:lineView];
}

- (void)viewWillAppear {
    [self setUpPropertiesSubviews];
}

#pragma mark action methods
- (IBAction)interactionEdit:(id)sender {
    [super interactionEdit:sender];
    [self changePropertiesSubviews];
}

- (IBAction)interactionUpdate:(id)sender {
    [super interactionUpdate:sender];
    [self changePropertiesSubviews];
}

- (IBAction)interactionCancel:(id)sender {
    [super interactionCancel:sender];
    [self changePropertiesSubviews];
}

@end