//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <CoreGraphics/CoreGraphics.h>
#import "INFormGeneratorViewOneColumn.h"
#import "INFormGeneratorViewTwoColumns.h"
#import "INFormGeneratorViewOption.h"
#import "INFormGeneratorViewFormatter.h"
#import "INDisplayedPropertiesModel.h"
#import "INFormGeneratorHelper.h"
#import "INFormFormatter.h"
#import "INLabel.h"

@interface INFormGeneratorViewOneColumn () {
}

@property INFormGeneratorViewFormatter *viewFormatter;
@property INFormGeneratorViewOption *viewOption;

@end

@implementation INFormGeneratorViewOneColumn {

}

@synthesize displayedObject, viewFormatter, viewOption;

- (id)initWithFrame:(CGRect)frame object:(id <INDisplayedPropertiesModel>)inDisplayedObject {
    return [self initWithFrame:frame object:inDisplayedObject formatter:[INFormGeneratorViewFormatter defaultOneColumnFormGeneratorViewFormatterWithFrame:frame]];
}

- (id)initWithFrame:(CGRect)frame object:(id <INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter {
    return [self initWithFrame:frame object:inDisplayedObject formatter:formatter option:[INFormGeneratorViewOption defaultFormGeneratorViewOption]];
}

- (id)initWithFrame:(CGRect)frame object:(id <INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter option:(INFormGeneratorViewOption *)option {
    self = [super initWithFrame:frame];
    if (self) {
        self.displayedObject = inDisplayedObject;
        self.viewFormatter = formatter;
        self.viewOption = option;
        self.currentMode = self.viewOption.initialFormMode;
        [self setUpView];

        [self addKeyboardObserver];
    }

    return self;
}

- (void)setUpView {
    self.delegate = self;
    [self setScrollEnabled:YES];

    if (!self.viewFormatter) {
        self.viewFormatter = [INFormGeneratorViewFormatter defaultOneColumnFormGeneratorViewFormatterWithFrame:self.frame];
    }

    [self setBackgroundColor:self.viewFormatter.backgroundColor];
    //[self setUpSubviews];
}

- (void)setUpSubviews {
    self.propertyTextFields = [@{} mutableCopy];

    [self setUpPropertiesSubviews:[self detailsKeysFromDisplayedObject:self.displayedObject]];
}

- (void)setUpPropertiesSubviews:(NSArray *)detailsKeys {
    CGSize labelSize = CGSizeMake(self.viewFormatter.titleFieldWidth, self.viewFormatter.textElementHeight);
    CGSize textFieldSize = CGSizeMake(self.viewFormatter.valueFieldWidth, self.viewFormatter.textElementHeight);
    float maxHeight = (labelSize.height > textFieldSize.height ? labelSize.height : textFieldSize.height);

    float calculationY = self.viewFormatter.y;
    float calculationX = self.viewFormatter.x;
    for (NSString *key in detailsKeys) {
        NSLog(@"key: %@", key);
        INLabel *label = [INFormGeneratorHelper createTitleLabel:CGRectMake(calculationX, calculationY, labelSize.width, labelSize.height) formFormatter:[INFormFormatter defaultNoBorderFormFormatter]];
        label.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(key, nil)];

        //next line
        calculationY += maxHeight + self.viewFormatter.ySpace;

        UIView *valueField;
        CGRect frame = CGRectMake(calculationX, calculationY, textFieldSize.width, textFieldSize.height);

        if (self.currentMode != show) {
            valueField = [INFormGeneratorHelper propertyUITextFieldForKey:self.displayedObject key:key frame:frame];
            ((UITextField *) valueField).delegate = self;
        } else {
            valueField = [INFormGeneratorHelper propertyUILabelForKey:self.displayedObject key:key frame:frame];
        }

        //add value-field to a map where the displayed value-fields are saved
        [self.propertyTextFields setObject:valueField forKey:key];

        [self addSubview:label];
        [self addSubview:valueField];

        calculationY += maxHeight + self.viewFormatter.ySpace;
    }
}

- (void)viewWillAppear {
    [self setUpSubviews];
}

- (void)changePropertiesSubviews {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }

    if (self.currentMode == update) {
        [self updateProperties:self.displayedObject];
        self.currentMode = show;
    }
    //finally renew view
    [self setUpSubviews];
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [super setActiveTextElement:textField];
}

#pragma mark action methods
- (IBAction)interactionEdit:(id)sender {
    [super interactionEdit:sender];
    [self changePropertiesSubviews];
}

- (IBAction)interactionUpdate:(id)sender {
    [super interactionUpdate:sender];
    [self changePropertiesSubviews];
}

- (IBAction)interactionCancel:(id)sender {
    [super interactionCancel:sender];
    [self changePropertiesSubviews];
}

@end