//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INFormGeneratorView.h"

@class INFormGeneratorViewFormatter;
@protocol INDisplayedPropertiesModel;


@interface INFormGeneratorViewOneColumn : INFormGeneratorView <UITextFieldDelegate, UIScrollViewDelegate>

@property id<INDisplayedPropertiesModel> displayedObject;

- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject;
- (id)initWithFrame:(CGRect)frame object:(id<INDisplayedPropertiesModel>)inDisplayedObject formatter:(INFormGeneratorViewFormatter *)formatter;

- (void)viewWillAppear;
@end