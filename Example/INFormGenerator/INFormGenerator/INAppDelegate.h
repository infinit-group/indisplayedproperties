//
//  INAppDelegate.h
//  INFormGenerator
//
//  Created by Nils Rohwedder on 31.05.13.
//  Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
