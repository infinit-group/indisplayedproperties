//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "INFormGeneratorViewOption.h"


@implementation INFormGeneratorViewOption {

}

@synthesize initialFormMode;

+ (INFormGeneratorViewOption *)defaultFormGeneratorViewOption {
    INFormGeneratorViewOption *defaultFormGeneratorViewOption = [[INFormGeneratorViewOption alloc] init];
    defaultFormGeneratorViewOption.initialFormMode = show;
    return defaultFormGeneratorViewOption;
}

@end