//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <Foundation/Foundation.h>

@interface INFormFormatter : NSObject

@property UIColor *backgroundColor;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) CALayer *layer;

- (id)initWithLayer:(CALayer *)inLayer backgroundColor:(UIColor *)inBackgroundColor font:(UIFont *)inFont textColor:(UIColor *)inTextColor;

+ (INFormFormatter *)defaultNoBorderFormFormatter;
@end