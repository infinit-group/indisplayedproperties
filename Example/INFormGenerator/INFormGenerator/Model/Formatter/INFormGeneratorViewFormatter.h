//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>


@interface INFormGeneratorViewFormatter : NSObject

@property CGRect frame;

@property float x; //starting x
@property float y; //starting y
@property float xBreak; //x breaking position
@property float yBreak; //y breaking position
@property float xSpace; //x space between labels
@property float ySpace; //y space between labels
@property float textElementHeight; //height of all text elements
@property float titleFieldWidth; // width of title label fields
@property float valueFieldWidth; // width of value fields

//layout properties

@property UIColor *backgroundColor;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;

+ (INFormGeneratorViewFormatter *)defaultOneColumnFormGeneratorViewFormatterWithFrame:(CGRect)frame;

+ (INFormGeneratorViewFormatter *)defaultTwoColumnFormGeneratorViewFormatterWithFrame:(CGRect)frame;

@end