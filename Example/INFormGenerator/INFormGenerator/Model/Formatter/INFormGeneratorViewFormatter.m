//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <CoreGraphics/CoreGraphics.h>
#import "INFormGeneratorViewFormatter.h"

@implementation INFormGeneratorViewFormatter {

}

@synthesize frame, x, y, xBreak, yBreak, xSpace, ySpace, titleFieldWidth, valueFieldWidth, textElementHeight, backgroundColor, font, textColor;

+ (INFormGeneratorViewFormatter *)defaultOneColumnFormGeneratorViewFormatterWithFrame:(CGRect)frame {
    INFormGeneratorViewFormatter *defaultFormGeneratorViewFormatter = [[INFormGeneratorViewFormatter alloc] init];
    float textElementHeight = 30.0f;

    defaultFormGeneratorViewFormatter.frame = frame;
    defaultFormGeneratorViewFormatter.x = 5.0f;
    defaultFormGeneratorViewFormatter.y = 5.0;
    defaultFormGeneratorViewFormatter.xSpace = 5.0f;
    defaultFormGeneratorViewFormatter.ySpace = 5.0f;
    defaultFormGeneratorViewFormatter.textElementHeight = textElementHeight;
    defaultFormGeneratorViewFormatter.titleFieldWidth = frame.size.width - (2 * defaultFormGeneratorViewFormatter.x);
    defaultFormGeneratorViewFormatter.valueFieldWidth = frame.size.width - (2 * defaultFormGeneratorViewFormatter.x) ;

    defaultFormGeneratorViewFormatter.font = [UIFont systemFontOfSize:12];
    defaultFormGeneratorViewFormatter.textColor = [UIColor blackColor];

    return defaultFormGeneratorViewFormatter;
}

+ (INFormGeneratorViewFormatter *)defaultTwoColumnFormGeneratorViewFormatterWithFrame:(CGRect)frame {
    INFormGeneratorViewFormatter *defaultFormGeneratorViewFormatter = [[INFormGeneratorViewFormatter alloc] init];
    float textElementHeight = 30.0f;

    defaultFormGeneratorViewFormatter.frame = frame;
    defaultFormGeneratorViewFormatter.x = 0.0f;
    defaultFormGeneratorViewFormatter.y = 5.0;
    defaultFormGeneratorViewFormatter.xSpace = 5.0f;
    defaultFormGeneratorViewFormatter.ySpace = 5.0f;
    defaultFormGeneratorViewFormatter.xBreak = frame.size.width / 2;
    defaultFormGeneratorViewFormatter.yBreak = frame.size.height - textElementHeight + 1;
    defaultFormGeneratorViewFormatter.textElementHeight = textElementHeight;
    defaultFormGeneratorViewFormatter.titleFieldWidth = (defaultFormGeneratorViewFormatter.xBreak * ((float) 1 / 3)) - (defaultFormGeneratorViewFormatter.x + defaultFormGeneratorViewFormatter.xSpace);
    defaultFormGeneratorViewFormatter.valueFieldWidth = (defaultFormGeneratorViewFormatter.xBreak * ((float) 2 / 3)) - (defaultFormGeneratorViewFormatter.xSpace);

    defaultFormGeneratorViewFormatter.font = [UIFont systemFontOfSize:12];
    defaultFormGeneratorViewFormatter.textColor = [UIColor blackColor];

    return defaultFormGeneratorViewFormatter;
}

@end