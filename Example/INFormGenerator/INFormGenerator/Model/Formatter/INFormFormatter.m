//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <QuartzCore/QuartzCore.h>
#import "INFormFormatter.h"


@implementation INFormFormatter {

}

@synthesize layer, backgroundColor, font, textColor;

- (id)initWithLayer :(CALayer *)inLayer backgroundColor:(UIColor *)inBackgroundColor font:(UIFont *)inFont textColor:(UIColor *)inTextColor {
    self = [super init];
    if (self) {
        self.layer = inLayer;
        self.backgroundColor = inBackgroundColor;
        self.font = inFont;
        self.textColor = inTextColor;
    }

    return self;
}

+ (INFormFormatter *)defaultNoBorderFormFormatter {
    CALayer *layer = [CALayer layer];
    return [[INFormFormatter alloc] initWithLayer:layer backgroundColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:12] textColor:[UIColor blackColor]];
}

@end