//
// Created by Nils Rohwedder on 17.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "INEmbeddedEmbeddedDummyPropertyModel.h"
#import "NSString+INPropertyString.h"


@implementation INEmbeddedEmbeddedDummyPropertyModel {

}

@synthesize embeddedEmbeddedStringProperty, embeddedEmbeddedIntProperty;

+ (NSArray *)displayedProperties:(NSString *)property {
    return @[
            [super displayProperty:[NSString propertyString: property, @"embeddedEmbeddedStringProperty", nil]], [super displayProperty:[NSString propertyString: property, @"embeddedEmbeddedIntProperty", nil]]
    ];
}

@end