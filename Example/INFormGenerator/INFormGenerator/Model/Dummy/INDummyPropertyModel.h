//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INDisplayedPropertiesModel.h"

@class INEmbeddedDummyPropertyModel;

@interface INDummyPropertyModel : INDisplayedPropertiesModel <INDisplayedPropertiesDelegate>

@property float floatProperty;
@property (nonatomic, strong) INEmbeddedDummyPropertyModel *embeddedProperty1;
@property (nonatomic, strong) NSDate *dateProperty;

@end