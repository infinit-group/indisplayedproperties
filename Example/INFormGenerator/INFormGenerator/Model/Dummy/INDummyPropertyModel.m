//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import "INDummyPropertyModel.h"
#import "INEmbeddedDummyPropertyModel.h"
#import "NSString+INPropertyString.h"

@implementation INDummyPropertyModel {

}

@synthesize floatProperty, embeddedProperty1, dateProperty;

+ (NSArray *)displayedProperties:(NSString *)property {
    return [
            @[
                    // adding the displayed properties "dateProperty" and "floatProperty"
                    [super displayProperty:[NSString propertyString:@"dateProperty", nil]], [super displayProperty:[NSString propertyString:@"floatProperty", nil]]
            ]
            arrayByAddingObjectsFromArray:
                    // and now adding the displayed properties for the property "embeddedProperty1" of the embedded class "INEmbeddedDummyPropertyModel"
                    [INEmbeddedDummyPropertyModel displayedProperties: [NSString propertyString: @"embeddedProperty1", nil]]
    ];
}

@end