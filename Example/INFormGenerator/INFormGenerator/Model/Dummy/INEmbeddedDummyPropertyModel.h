//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INDisplayedPropertiesModel.h"

@class INEmbeddedEmbeddedDummyPropertyModel;

@interface INEmbeddedDummyPropertyModel : INDisplayedPropertiesModel <INDisplayedPropertiesDelegate>

@property (nonatomic, strong) NSString *embeddedStringProperty;
@property int embeddedIntProperty;

@property (nonatomic, strong) INEmbeddedEmbeddedDummyPropertyModel *embeddedEmbeddedDummyPropertyModel;

@end