//
// Created by Nils Rohwedder on 17.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "INEmbeddedDummyPropertyModel.h"


@interface INEmbeddedEmbeddedDummyPropertyModel : INEmbeddedDummyPropertyModel<INDisplayedPropertiesDelegate>

@property (nonatomic, strong) NSString *embeddedEmbeddedStringProperty;
@property int embeddedEmbeddedIntProperty;

@end