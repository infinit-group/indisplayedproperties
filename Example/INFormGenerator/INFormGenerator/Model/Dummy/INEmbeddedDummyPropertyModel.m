//
// Created by Nils Rohwedder on 31.05.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import "INEmbeddedDummyPropertyModel.h"
#import "INEmbeddedEmbeddedDummyPropertyModel.h"
#import "NSString+INPropertyString.h"

@implementation INEmbeddedDummyPropertyModel {

}

@synthesize embeddedStringProperty, embeddedIntProperty, embeddedEmbeddedDummyPropertyModel;

+ (NSArray *)displayedProperties:(NSString *)property {
    return [@[
            [super displayProperty:[NSString propertyString:property, @"embeddedStringProperty", nil]], [super displayProperty:[NSString propertyString: property, @"embeddedIntProperty", nil]]
    ] arrayByAddingObjectsFromArray:
            [INEmbeddedEmbeddedDummyPropertyModel displayedProperties: [NSString propertyString: property, @"embeddedEmbeddedDummyPropertyModel", nil] ]
    ];
}

@end