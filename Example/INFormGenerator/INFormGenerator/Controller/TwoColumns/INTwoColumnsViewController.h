//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class INDummyPropertyModel;


@interface INTwoColumnsViewController : UIViewController

@property (nonatomic, strong) INDummyPropertyModel *dummyPropertyModel;

@end