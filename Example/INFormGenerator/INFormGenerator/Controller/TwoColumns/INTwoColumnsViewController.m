//
// Created by Nils Rohwedder on 03.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "INTwoColumnsViewController.h"
#import "INFormGeneratorViewTwoColumns.h"
#import "INDummyPropertyModel.h"
#import "INEmbeddedDummyPropertyModel.h"

@interface INTwoColumnsViewController() {
    INFormGeneratorViewTwoColumns *formGeneratorView;
    UIBarButtonItem *editButton;
    UIBarButtonItem *updateButton;
    UIBarButtonItem *cancelButton;
}
@end

@implementation INTwoColumnsViewController {

}

@synthesize dummyPropertyModel;

- (void)viewDidLoad {
    [super viewDidLoad];

    formGeneratorView = [[INFormGeneratorViewTwoColumns alloc] initWithFrame:self.view.frame object:self.dummyPropertyModel];

    editButton = [[UIBarButtonItem alloc] initWithTitle:(@"Bearbeiten") style:UIBarButtonItemStyleDone target:self action:@selector(interactionEdit:)];
    self.navigationItem.rightBarButtonItem = editButton;

    [self.view addSubview:formGeneratorView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [formGeneratorView viewWillAppear];
}

- (IBAction)interactionEdit:(id)sender {
    [formGeneratorView interactionEdit:sender];
    updateButton = [[UIBarButtonItem alloc] initWithTitle:(@"Speichern") style:UIBarButtonItemStyleDone target:self action:@selector(interactionUpdate:)];
    self.navigationItem.rightBarButtonItem = updateButton;

    cancelButton = [[UIBarButtonItem alloc] initWithTitle:(@"Abbruch") style:UIBarButtonItemStyleDone target:self action:@selector(interactionCancel:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (IBAction)interactionUpdate:(id)sender {
    [formGeneratorView interactionUpdate:sender];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = editButton;
}

- (IBAction)interactionCancel:(id)sender {
    [formGeneratorView interactionCancel:sender];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = editButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end