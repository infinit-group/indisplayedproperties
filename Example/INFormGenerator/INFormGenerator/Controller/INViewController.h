//
//  INViewController.h
//  INFormGenerator
//
//  Created by Nils Rohwedder on 31.05.13.
//  Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INViewController : UIViewController{
    IBOutlet UIButton *oneColumnButton;
    IBOutlet UIButton *twoColumnsButton;
}

- (IBAction)interactionOneColumnButtonTapped:(id)sender;

- (IBAction)interactionTwoColumnsButtonTapped:(id)sender;
@end
