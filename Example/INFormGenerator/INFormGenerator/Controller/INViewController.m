//
//  INViewController.m
//  INFormGenerator
//
//  Created by Nils Rohwedder on 31.05.13.
//  Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//

#import "INViewController.h"
#import "INFormGeneratorViewTwoColumns.h"
#import "INDummyPropertyModel.h"
#import "INEmbeddedDummyPropertyModel.h"
#import "INTwoColumnsViewController.h"
#import "INOneColumnViewController.h"
#import "INEmbeddedEmbeddedDummyPropertyModel.h"

@interface INViewController (){
    INOneColumnViewController *oneColumnViewController;
    INTwoColumnsViewController *twoColumnsViewController;
    INDummyPropertyModel *dummyPropertyModel;
}
@end

@implementation INViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    dummyPropertyModel = [[INDummyPropertyModel alloc] init];
    dummyPropertyModel.dateProperty = [NSDate new];
    dummyPropertyModel.floatProperty = 1.5;
    INEmbeddedDummyPropertyModel *embeddedDummyPropertyModel1 = [[INEmbeddedDummyPropertyModel alloc] init];
    embeddedDummyPropertyModel1.embeddedIntProperty = 1;
    embeddedDummyPropertyModel1.embeddedStringProperty = @"Embedded String1";
    dummyPropertyModel.embeddedProperty1 = embeddedDummyPropertyModel1;

    INEmbeddedEmbeddedDummyPropertyModel *embeddedEmbeddedDummyPropertyModel = [[INEmbeddedEmbeddedDummyPropertyModel alloc] init];
    embeddedEmbeddedDummyPropertyModel.embeddedEmbeddedStringProperty = @"Embedded embedded String";
    embeddedEmbeddedDummyPropertyModel.embeddedEmbeddedIntProperty = 2;

    dummyPropertyModel.embeddedProperty1.embeddedEmbeddedDummyPropertyModel = embeddedEmbeddedDummyPropertyModel;

    oneColumnViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"oneColumn"];
    oneColumnViewController.dummyPropertyModel = dummyPropertyModel;
    twoColumnsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"twoColumns"];
    twoColumnsViewController.dummyPropertyModel = dummyPropertyModel;

    [self.view addSubview:oneColumnButton];
    [self.view addSubview:twoColumnsButton];
}

- (IBAction)interactionOneColumnButtonTapped:(id)sender {
    [self.navigationController pushViewController:oneColumnViewController animated:YES];
}

- (IBAction)interactionTwoColumnsButtonTapped:(id)sender {
    [self.navigationController pushViewController:twoColumnsViewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
