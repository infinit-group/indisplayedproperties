# About this Library

This is an Implementation to easily read and update properties in a UIView. 

## How to use this library

All you need to do, is to implement the class "INDisplayedPropertiesModel" and override the Method: 

"+ (NSArray *)displayedProperties:(NSString *)property"

Then you can iterate over the properties you want to display in order of their appearance, returned from the method "displayedProperties".

## Using the implemented "Read" and "Update"-Functionality

### Using default iOS-Data-Types 
When using the "INDisplayedPropertiesModel" for the "Read"- and "Update"-Functionality you need to be sure to follow the naming convention of the library. 
Therefore I added the Categorie "INPropertyString" adding Helper-Methods to the "NSString"-Class. 
You can easily create your property-string following the naming convention by using the result of calling: 
"[super displayProperty:[NSString propertyString: @"#propertyName#", nil]]" with replacing #propertyName# with the real name of your property. 
!Be sure that the String-Value is exact like the name of the property.

### Using individual Data-Types with a hierarchy
When using properties of your own data type also implement the class "INDisplayedPropertiesModel" for your subclass just like explained above. Then add the result to the Array returning in your superclass. 
The created property-string in the subclass then should look like "[super displayProperty:[NSString propertyString: property, @"#propertyName#", nil]]"

##Example
You can find an example (using properties as well as embedded properties) using this library in the "Example" folder.