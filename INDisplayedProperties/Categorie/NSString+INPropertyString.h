//
// Created by Nils Rohwedder on 17.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@protocol INDisplayedPropertiesModel;

@interface NSString (INPropertyString)

+ (NSString *)propertyString:(NSString *)property, ... NS_REQUIRES_NIL_TERMINATION;

+ (NSString *)propertyStringWithClass:(Class <INDisplayedPropertiesModel>)class property:(NSString *)property;

+ (BOOL)propertyStringWithClassBelongsToClass:(Class)class property:(NSString *)propertyWithClass;

+ (id)valueForPropertyString:(id <INDisplayedPropertiesModel>)propertyObject property:(NSString *)property;

+ (void)updatePropertyForString:(id <INDisplayedPropertiesModel>)o property:(NSString *)property value:(id)value;

@end