//
// Created by Nils Rohwedder on 13.06.13.
// Copyright (c) 2013 Nils Rohwedder. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@protocol INDisplayedPropertiesDelegate <NSObject>

+ (NSArray *)displayedProperties:(NSString *)property;

@end